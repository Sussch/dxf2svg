#!/usr/bin/env python

"""
Original by Tomasz Lenarcik (apendua), apendua@gmail.com.
The original is accessible at https://github.com/apendua/dxf2svg

Refactored and updated by Sussch.
"""

from __future__ import print_function
import dxfgrabber
import argparse
import math
import sys
import os

# http://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
def p_warning(*objs):
	print("WARNING: ", *objs, file=sys.stderr)

def p_error(*objs):
	print("ERROR: ", *objs, file=sys.stderr)

class DXF2SVG():
	# SVG TEMPLATES

	SVG_PREAMBLE = \
	'<svg xmlns="http://www.w3.org/2000/svg" ' \
	'version="1.1" viewBox="{0} {1} {2} {3}">\n'

	SVG_MOVE_TO = 'M {0} {1} '
	SVG_LINE_TO = 'L {0} {1} '
	SVG_ARC_TO  = 'A {0} {1} {2} {3} {4} {5} {6} '

	SVG_PATH = \
	'<path d="{0}" fill="none" stroke="{1}" stroke-width="{2:.2f}" />\n'

	SVG_LINE = \
	'<line x1="{0}" y1="{1}" x2="{2}" y2="{3}" stroke="{4}" stroke-width="{5:.2f}" />\n'

	SVG_CIRCLE = \
	'<circle cx="{0}" cy="{1}" r="{2}" stroke="{3}" stroke-width="{4}" fill="none" />\n'

	def __init__(self):
		self.dxf_data = None
		self.dxf_extent = [0, 0, 0, 0]
		
		self.svg_stroke_width = 0.01
		self.svg_str = ''
		self.svg_block_str = {}

	# SVG DRAWING HELPERS
	
	def updateViewbox(self, point):
		if point[0] < self.dxf_extent[0]:
			self.dxf_extent[0] = point[0]
		if point[1] < self.dxf_extent[1]:
			self.dxf_extent[1] = point[1]
		if point[0] > self.dxf_extent[2]:
			self.dxf_extent[2] = point[0]
		if point[1] > self.dxf_extent[3]:
			self.dxf_extent[3] = point[1]

	def angularDifference(self, startangle, endangle):
		result = endangle - startangle
		while result >= 360:
			result -= 360
		while result < 0:
			result += 360
		return result

	def pathStringFromPoints(self, points):
		path_str = DXF2SVG.SVG_MOVE_TO.format(*points[0])
		self.updateViewbox(points[0])
		
		for i in range(1, len(points)):
			path_str += DXF2SVG.SVG_LINE_TO.format(*points[i])
			self.updateViewbox(points[i])
		
		return path_str
	
	def pathStringFromPolyface(self, polyface):
		polyface_str = ''
		
		for subface in polyface:
			# All face paths are closed
			path_str = self.pathStringFromPoints(subface) + 'Z'
			polyface_str += DXF2SVG.SVG_PATH.format(path_str, 'black', 
										   self.svg_stroke_width)
		
		return polyface_str
	
	# LOADING DXF
	
	def loadDXF(self, filepath):
		# grab data from file
		self.dxf_data = dxfgrabber.readfile(filepath)

	# CONVERTING TO SVG

	def handleEntity(self, e):
		entity_str = ''
		# TODO: handle colors and thinckness
		# TODO: handle elipse and spline and some other types

		if e.dxftype == 'LINE':
			entity_str += DXF2SVG.SVG_LINE.format(
				e.start[0], e.start[1], e.end[0], e.end[1],
				'black', self.svg_stroke_width
				)
			
			self.updateViewbox(e.start)
			self.updateViewbox(e.end)

		elif e.dxftype == 'POLYLINE':
			path_str = self.pathStringFromPoints(e)
			if e.is_closed:
				path_str += 'Z'
			entity_str += DXF2SVG.SVG_PATH.format(path_str, 'black',
										 self.svg_stroke_width)
		
		elif e.dxftype == 'POLYFACE':
			entity_str += self.pathStringFromPolyface(e)
			
		elif e.dxftype == 'CIRCLE':
			entity_str += DXF2SVG.SVG_CIRCLE.format(e.center[0], e.center[1],
				e.radius, 'black', self.svg_stroke_width)
			
			self.updateViewbox((e.center[0] - e.radius, e.center[1] - e.radius))
			self.updateViewbox((e.center[0] + e.radius, e.center[1] + e.radius))

		elif e.dxftype == 'ARC':
			# compute end points of the arc
			x1 = e.center[0] + e.radius * math.cos(math.pi * e.startangle / 180)
			y1 = e.center[1] + e.radius * math.sin(math.pi * e.startangle / 180)
			x2 = e.center[0] + e.radius * math.cos(math.pi * e.endangle / 180)
			y2 = e.center[1] + e.radius * math.sin(math.pi * e.endangle / 180)

			pathString  = DXF2SVG.SVG_MOVE_TO.format(x1, y1)
			pathString += DXF2SVG.SVG_ARC_TO.format(e.radius, e.radius, 0,
				int(angularDifference(e.startangle, e.endangle) > 180), 1, x2, y2)

			entity_str += DXF2SVG.SVG_PATH.format(pathString, 'black', 
										 self.svg_stroke_width)
			
			self.updateViewbox((e.center[0] - e.radius, e.center[1] - e.radius))
			self.updateViewbox((e.center[0] + e.radius, e.center[1] + e.radius))
		
		elif e.dxftype == 'INSERT':
			entity_str += self.svg_block_str[e.name]
		
		else:
			p_error("Support for entity type \"{}\" is not supported yet.".\
				format(e.__class__.__name__))

		return entity_str
		#end: handleEntity

	def saveSVG(self, filepath):
		self.svg_str = ''
		
		# NOTE:: $EXTMIN, $EXTMAX are not specified in all DXF files.
		# In the case that they're missing, we'll be calculating
		# the SVG viewbox while handling the entities.
				
		# Format blocks to strings
		for block in self.dxf_data.blocks:
			self.svg_block_str[block.name] = ''
			
			for entity in block:
				self.svg_block_str[block.name] += self.handleEntity(entity)
		
		# Format entities to string
		for entity in self.dxf_data.entities:
			# Entity table is correct?
			if entity.layer in self.dxf_data.layers:
				layer = self.dxf_data.layers[entity.layer]
				if layer.on and not layer.frozen:
					self.svg_str += self.handleEntity(entity)
			# Also draw entities that are not in any layers.
			else:
				p_warning("Entity {} is supposed to be on layer {}, which" \
					" is not listed in LayerTable.".\
						format(entity.name, entity.layer))
				
				self.svg_str += self.handleEntity(entity)
		
		# Use our calculated viewbox by default
		extent = self.dxf_extent
		# If extent is specified in the DXF header use that
		# instead of our calculated values.
		if '$EXTMIN' in self.dxf_data.header \
			and '$EXTMAX' in self.dxf_data.header:
			extent = self.dxf_data.header['$EXTMIN'] + \
				self.dxf_data.header['$EXTMAX']
		
		# TODO: alert if the file already exists
		# convert and save to svg
		with open(filepath, 'w') as svg_file:
			# Write preamble
			# TODO: also handle groups
			svg_file.write(DXF2SVG.SVG_PREAMBLE.format(
				extent[0], extent[1],
				extent[2] - extent[0],
				extent[3] - extent[1]
				))
			
			# Write SVG content
			svg_file.write(self.svg_str)
			
			# Write postamble
			svg_file.write('</svg>\n')
			
		#end: saveToSVG

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Convert From DXF to SVG.',
								  prog='dxf2svg')
	parser.add_argument('dxf_path',
					 help='Path to the DXF file to convert.'
					 )
	parser.add_argument('svg_path',
					 help='Path to the SVG output file.', 
					 nargs='?', default=''
					 )
	parser.add_argument('-w', '--stroke-width',
					 metavar='stroke_width',
					 help='Default stroke width.',
					 default=0.1, type=float, 
					 required=False
					 )
	
	args = parser.parse_args()

	if args.svg_path == '':
		args.svg_path = '.'.join(args.dxf_path.split('.')[:-1] + ['svg'])
	
	converter = DXF2SVG()
	converter.svg_stroke_width = args.stroke_width
	converter.loadDXF(args.dxf_path)
	converter.saveSVG(args.svg_path)

	#end: __main__

